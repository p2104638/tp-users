import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent {
  name: string = '';
  password: string = '';
  errorMessage: string = '';

  constructor(private router: Router, private userService: UserService) {}

  login() {
    // Vérifie si le nom d'utilisateur existe
    const user = this.userService.getUserByUsername(this.name);

    // @ts-ignore
    if (user) {
      this.router.navigate(["/home"]);
    } else {
      this.errorMessage = 'Nom d\'utilisateur introuvable essayez avec "John Doe"';
    }

  }
}

