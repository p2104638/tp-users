# User APP

## Description
Une application Angular de gestion des utilisateurs.

## Fonctionnalités
- Affichage de la liste des utilisateurs.
- Ajout, modification et suppression d'utilisateurs.
- Connexion avec vérification du nom d'utilisateur.

## Prérequis
- Node.js et npm installés sur votre machine.
- Angular CLI installé (`npm install -g @angular/cli`).

## Installation
1. Clonez ce référentiel : `git clone`
2. Accédez au répertoire du projet : `cd nom-du-projet`
3. Installez les dépendances : `npm install`

## Lancement de l'Application
- Utilisez la commande : `ng serve`
- L'application sera accessible à l'adresse http://localhost:4200/ dans votre navigateur.

## Connexion
- Accédez à la page de connexion.
- Saisissez un nom d'utilisateur existant : John Doe en est un.
- Appuyez sur le bouton de connexion.

---

## Structure du Projet
- **src/app:** Contient les composants de l'application.
- **src/app/shared:** Contient les services pour la gestion des données.

## Technologies Utilisées
- Angular
- RxJS

## Auteur
Mathis Toinon
